#include <iostream>

class Foo
{
public:
    // ...

private:
    // Never defined
    Foo(const Foo&);
    Foo& operator=(const Foo&);
};

int main()
{

}
