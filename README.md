# c++14-otris

Presentation slides for C++ beginners about C++11/14.


## License

reveal.js is MIT licensed

Copyright (C) 2015 Hakim El Hattab, http://hakim.se
